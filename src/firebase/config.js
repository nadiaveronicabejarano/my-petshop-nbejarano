// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyALdtLy_3q4fkEkJkRdNZ5UyPIDzkkpAuM",
  authDomain: "my-awesome-petshop-a7af5.firebaseapp.com",
  projectId: "my-awesome-petshop-a7af5",
  storageBucket: "my-awesome-petshop-a7af5.appspot.com",
  messagingSenderId: "26759278918",
  appId: "1:26759278918:web:f9c4e81720eb4ef7d33547"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default function getFireStoreBase(){
    return app
}