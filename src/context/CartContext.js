import { createContext, useContext, useState } from "react";

const CartContext = createContext([])

export const useCartContext = () => useContext(CartContext)

function CartContextProvider( {children} ) {
    const [cartList, setCartList] = useState([])

    const addToCart = ( product ) => {
        let addToCart = true
        if ( !isInCart(product.id) ) {
            setCartList( [ ...cartList, product ] )
        } else {
            addToCart = updateQty( product )
        }
        return addToCart
    }

    const clear = () => {
        setCartList([])
    }

    const isInCart = ( productId ) => {
        let isInCart = false;
        if ( typeof cartList.find( product => product.id === productId ) !== "undefined" ) {
            isInCart = true;
        }
        return isInCart
    }

    const removeItem = ( productId ) => {
        let itemToDeleteIndex = cartList.findIndex( ( product ) => product.id === productId );
        if ( itemToDeleteIndex !== -1 ) {
            cartList.splice( itemToDeleteIndex, 1 )
            setCartList( [...cartList] )
        }
    }

    const updateQty = ( product ) => {
        let itemToUpdateIndex   = cartList.findIndex( ( prod ) => prod.id === product.id );
        let productInCart       = cartList[ itemToUpdateIndex ]
        if ( product.qty <= ( productInCart.stockAvailable - productInCart.qty ) ) {
            productInCart.qty += product.qty
            return true
        } else {
            return false
        }
    }

    const getQtyTotal = () => {
        let qtyTotal = 0
        cartList.forEach( product => {
            qtyTotal += product.qty
        })
        return qtyTotal
    }

    const getTotal = () => {
        let cartTotal = 0
        cartList.forEach( product => {
            cartTotal += product.price * product.qty
        } )
        return cartTotal
    }

    return (
        <CartContext.Provider value={{
            cartList,
            addToCart,
            clear,
            removeItem,
            getQtyTotal,
            getTotal
        }}>
            {children}
        </CartContext.Provider>
    )
}

export default CartContextProvider