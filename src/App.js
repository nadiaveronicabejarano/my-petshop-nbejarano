import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './App.css';
import NavBar from './components/NavBar';
import 'materialize-css'; 
import ItemListContainer from './components/ItemListContainer';
import ItemDetailContainer from './components/ItemDetailContainer';
import CartContextProvider from './context/CartContext';
import CartContainer from './components/Cart';


function App() {
  return (
      <CartContextProvider>
        <BrowserRouter>
            <NavBar></NavBar>
          <Routes>
            <Route path="/" element={<ItemListContainer greeting="Hola ReactJs" />} />
            <Route path="/alimento/:category" element={<ItemListContainer greeting="Hola ReactJs" />} />
            <Route path="/alimento/:category" element={<ItemListContainer greeting="Hola ReactJs" />} />
            <Route path="/detail/:productId" element={<ItemDetailContainer />} />
            <Route path="/cart" element={ <CartContainer/> } />
          </Routes>
        </BrowserRouter>
      </CartContextProvider>
  );
}

export default App;
