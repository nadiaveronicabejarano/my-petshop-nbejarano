
const products = [
    {
        'id': "1",
        'title':"Indoor Long Hair",
        'description':"Alimento para gatos adultos de pelo largo que viven en el interior del hogar. De 1 a 7 años de edad.",
        'price':"$2000",
        'pictureUrl':"https://cdn.royalcanin-weshare-online.io/KSLNs2sBaxEApS7LpyBf/v2/ar-l-producto-indoor-longhair-feline-health-nutrition-seco",
        'stockAvailable':12,
        'category': "gatos"
    },
    {
        'id': "2",
        'title':"Weight Care",
        'description':"Alimento para gatos adultos con tendencia al sobrepeso.",
        'price':"$2000",
        'pictureUrl':"https://cdn.royalcanin-weshare-online.io/QOfHFHIBaPOZra8qQ3Oo/v11/ar-l-producto-weight-care-feline-care-nutrition-seco",
        'stockAvailable':11,
        'category': "gatos"
    },
    {
        'id': "3",
        'title':"Persian Adult",
        'description':"Alimento especialmente formulado para gatos adultos de raza Persa - A partir de los 12 meses de edad.",
        'price':"$2000",
        'pictureUrl':"https://cdn.royalcanin-weshare-online.io/22nhs2sBG95Xk-RBRPvv/v4/ar-l-producto-persian-feline-breed-nutrition-seco",
        'stockAvailable':9,
        'category': "gatos"
    },
    {
        'id': "4",
        'title':"Fit",
        'description':"Alimento para gatos adultos con peso ideal, actividad física moderada y que poseen acceso al exterior. De 1 a 7 años de edad.",
        'price':"$2000",
        'pictureUrl':"https://cdn.royalcanin-weshare-online.io/1mnTs2sBG95Xk-RBuvto/v4/ar-l-producto-fit-feline-health-nutrition-seco",
        'stockAvailable':5,
        'category': "gatos"
    },
    {
        'id': "5",
        'title':"Maxi Castrados",
        'description':"Luego de la castración el metabolismo de tu perro se enlentece, utiliza menos energía y podés notarlo más hambriento. A partir de la castración, es clave mantenerlo en una óptima condición corporal. Alimento para perros adultos castrados de talla grande (de 26 a 44 kg). A partir de los 15 meses de edad. Recomendado para perros con tendencia al sobrepeso.",
        'price':"$2150",
        'pictureUrl':"https://cdn.royalcanin-weshare-online.io/POcYyXMBaPOZra8qj497/v7/ar-l-producto-maxi-castrados-sterilised-canine-care-nutrition-seco",
        'stockAvailable':12,
        'category': "perros"
    },
    {
        'id': "6",
        'title':"Golden Retriever Adult",
        'description':"Alimento para perros adultos de raza Golden Retriever – A partir de los 15 meses de edad.",
        'price':"$2250",
        'pictureUrl':"https://cdn.royalcanin-weshare-online.io/oj8183oBRYZmsWpc-7eK/v5/ar-l-producto-golden-retriever-adulto-breed-healt-nutrition-seco",
        'stockAvailable':3,
        'category': "perros"
    },
    {
        'id': "7",
        'title':"Medium Castrados",
        'description':"Luego de la castración el metabolismo de tu perro se enlentece, utiliza menos energía y podés notarlo más hambriento. A partir de la castración, es clave mantenerlo en una óptima condición corporal. Alimento para perros adultos castrados de talla mediana (11-25 kg). A partir de los 12 meses de edad. Recomendado para perros con tendencia al sobrepeso.",
        'price':"$2400",
        'pictureUrl':"https://cdn.royalcanin-weshare-online.io/NOcPyXMBaPOZra8qLY8V/v7/ar-l-producto-medium-castrados-sterilised-canine-care-nutrition-seco",
        'stockAvailable':20,
        'category': "perros"
    },
    {
        'id': "8",
        'title':"Mini Relax Care",
        'description':"Alimento para perros adultos y maduros de talla pequeña (peso hasta 10 kg) en un entorno cambiante – A partir de los 10 meses de edad.",
        'price':"$2700",
        'pictureUrl':"https://cdn.royalcanin-weshare-online.io/I-f7yHMBaPOZra8qNo_M/v7/ar-l-producto-mini-relax-care-canine-care-nutrition-seco",
        'stockAvailable':9,
        'category': "perros"
    }
]

export const getFetch = new Promise( ( res ) => {
    setTimeout( () => { 
        res(products)
    }, 2000)
})