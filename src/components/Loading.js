import { Row, Col } from 'react-materialize'

function Loading() {
    return (
        <Row>
            <Col m={6} s={12} l={4} offset="l4">
                <img src="https://img.pikbest.com/png-images/20190918/cartoon-snail-loading-loading-gif-animation_2734139.png!bw340"></img>
            </Col>
        </Row>
    )
}

export default Loading