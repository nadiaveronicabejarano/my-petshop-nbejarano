import { useState } from "react"
import { Button, Col, Row } from "react-materialize"
import ItemCount from "./ItemCount"
import { Link } from "react-router-dom"
import { useCartContext } from "../context/CartContext"



function ItemDetail( {product} ) {

    const [showItemCount, setShowItemCount] = useState( false )
    const { addToCart } = useCartContext()

    const onAdd = ( stock ) => {
        setShowItemCount( addToCart( { ...product, "qty": stock } ) )
    }

    return (
        <Row>
            <Col l={6} >
                <img src={product.pictureUrl} className="responsive-img" alt="avatar" width="90%" height="90%" />
            </Col>
            <Col l={6} >
                <Row>
                    <h3 style={{color:"#ee6e73"}}>
                        {product.title}
                    </h3>
                </Row>
                <Row>{product.description}</Row>
                <Row>
                    {product.price}
                </Row>
                <Row>
                    { !showItemCount ?
                        <ItemCount stockAvailable={product.stockAvailable} initial={1} onAdd={onAdd} />
                        :
                        <Link to="/cart">
                            <Button>Terminar la compra</Button>
                        </Link>
                    }
                </Row>
            </Col>
        </Row>
    )
}

export default ItemDetail