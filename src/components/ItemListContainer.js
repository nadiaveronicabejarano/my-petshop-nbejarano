import { useEffect, useState } from "react"
import { getFetch } from "../helpers/getFetch"
import ItemList from "./ItemList"
import { useParams } from 'react-router-dom'
import Loading from "./Loading"
import { getFirestore, collection, getDocs, query, where } from "firebase/firestore"

function ItemListContainer({ greeting }) {

    const [ products, setProducts ] = useState ([])
    const [ loading, setLoading ]   = useState (true)
    const { category }              = useParams()
    const itemsPerRow               = 4
    const db                        = getFirestore()

    const formatProducts = ( allProducts, rowsNr ) => {
        let [...products]  = allProducts;
        var result = [];
        while (products.length) {
            result.push(products.splice(0, rowsNr));
        }
        return result;
    }

    const getProductsByCategoryId = ( categoryId ) => {
        const firestoreCollection = query( collection( db, "products"), where("category","==",categoryId ) )
       requestToFireStore( firestoreCollection)
    }

    const getAllProducts = () => {
        const firestoreCollection = collection( db, "products")
        requestToFireStore(firestoreCollection)
    }

    const requestToFireStore = collection => {
        let productsCollection = []
        getDocs( collection )
            .then( prods => {
                if ( prods.size !== 0  ) {
                    prods.docs.map(doc => {
                        productsCollection.push({id: doc.id, ...doc.data()})
                    })
                    setProducts(formatProducts(productsCollection, itemsPerRow))
                }
            } )
            .finally( () => setLoading( false))
    }
 
    useEffect( () => {
        if ( category ) {
            getProductsByCategoryId( category )
        } else {
            getAllProducts()
        }
    },[ category ])
    
    return (
        <>
            { loading ? 
                <Loading/> : 
                <ItemList products={products}/> }
        </>
        
    )
}

export default ItemListContainer