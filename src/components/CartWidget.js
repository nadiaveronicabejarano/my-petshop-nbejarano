import { Badge, Icon } from 'react-materialize'
import { useCartContext } from "../context/CartContext"

function CartWidget() {
    const { getQtyTotal } = useCartContext()
    return (
        <>
            <Icon className="material-icons left">shopping_cart</Icon>{parseInt(getQtyTotal())}
        </>
    )
}

export default CartWidget