import { useEffect } from "react"
import { Button, Col, Row } from "react-materialize"
import { useCartContext } from "../context/CartContext"
import { Link } from "react-router-dom"
import {collection, getFirestore, addDoc, doc} from "firebase/firestore";

function CartContainer() {
    const { cartList, removeItem, clear, getTotal } = useCartContext()
    const db = getFirestore()

    const createOrder = () => {
        let order = {}
        order.buyer = {
            name: "Nadia Bejarano",
            phone: "2323423423423423",
            email: "nadiaveronicabejarano@gmail.com"
        }
        order.total = getTotal()
        order.date = getCurrentDate()
        order.items = cartList.map( product => {
            const {id, title} = product
            const price = product.price
            const qtySold = product.qty
            return {id, title, price, qtySold}
        } )
        sendOrderToFirestore(order)
    }

    const getCurrentDate = () => {
        const today = new Date();
        return today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    }

    const sendOrderToFirestore = order => {
        const ordersCollection = collection(db, "orders")
        addDoc(ordersCollection, order)
            .catch( err => console.log(err) )
            .then( () => updateItemsStock(order) )
            .finally( clear() )
    }

    const updateItemsStock = order => {
        console.log("Actualizar el stock ",order)
    }

    return (
        <>
        {cartList.length > 0 && 
            <>
                <Row>
                    <Col l={10} offset="l2"><h4>Carrito</h4></Col>
                </Row>
                <Row>
                    <Col l={8} offset="l2">
                        {cartList.map( product  => (
                            <Row key={product.id}>
                                <Col l={2}>
                                    <img src={product.pictureUrl} width="50%" height="50%"></img>
                                </Col>
                                <Col l={8}>
                                    <Row><h6>{ product.title }</h6></Row>
                                    <Row><span>{ product.description }</span></Row>
                                    <Row><span>{ product.price }</span></Row>
                                    <Row><span>Cantidad { product.qty }</span></Row>
                                </Col>
                                <Col l={2}>
                                    <Button onClick={() => removeItem(product.id)} style={{background:"#ee6e73"}} className="right" >Eliminar</Button>
                                </Col>
                            </Row>    
                        ))}
                    </Col>
                </Row> 
                <Row>
                    <Col l={4} offset="l2">
                        <Button onClick={clear} style={{background:"#ee6e73"}} >Vaciar carrito</Button>
                    </Col>
                    <Col l={4} >
                        <Button onClick={() => createOrder()} className="right" style={{background:"#ee6e73"}}>Comprar</Button>
                    </Col>
                </Row>
            </>
        }
        {cartList.length === 0 && 
            <>
                <Row className="center-align">
                    <Col l={12}>
                        <p>El Carrito esta vacio.</p>
                    </Col>
                </Row>
                <Row className="center-align">
                    <Col l={12}>
                        <Link to="/">
                            <Button style={{background:"#ee6e73"}}>
                                Ir al catalogo
                            </Button>
                        </Link>
                    </Col>
                </Row>
            </>
        }
        </>
    )
}

export default CartContainer