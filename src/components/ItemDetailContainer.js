import { useEffect, useState } from "react"
import { getFetch } from "../helpers/getFetch"
import ItemDetail from "./ItemDetail"
import { useParams } from "react-router-dom"  
import Loading from "./Loading"
import { getDoc, doc, getFirestore } from "firebase/firestore";


function ItemDetailContainer() {

    const [ product, setProduct ]   = useState ( {} )
    const [ loading, setLoading ]   = useState (true)
    const { productId }             = useParams()
    const db                        = getFirestore()

    useEffect( () => {
        const queryDb = doc( db, "products", productId)
        getDoc( queryDb )
            .then( prod => {
                setProduct({ id: prod.id, ...prod.data() })
            } )
            .finally( () => setLoading( false))
    }, [ productId ])
    console.log(product)
    return( 
        <> 
            { loading ? 
            <Loading /> : 
            <ItemDetail product={ product } /> } 
        </>
    )
}

export default ItemDetailContainer