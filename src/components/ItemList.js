import { Row } from "react-materialize"
import Item from "./Item"

function ItemList( {products} ){

    return (
        <>
            {products.map( (product,index) => (
                <Row key={index} >
                    {product.map( (prod)  => <Item product={prod} key={prod.id}/>)}
                </Row>
            ))}
        </>
    )
}

export default ItemList