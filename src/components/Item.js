import { Button, Card, Col, Row } from "react-materialize"
import { Link } from "react-router-dom"

function Item( { product } ) {
    return (
        <Col m={6} s={12} l={3}> 
            <Card
                key={product.id}
                className="white darken-1"
                textClassName="black-text"
                title={product.title}
            >
                <Row key={product.id + "_1"}>
                    <Col s={8} m={8} l={8} offset="s2 m2 l2">
                        <img src={product.pictureUrl} className="responsive-img" alt="avatar"/>
                    </Col>
                </Row>
                <Row key={product.id + "_2"} className="center-align">
                    <Col l={12}>
                        <Link to={ "/detail/" + product.id }>
                            <Button style={{background:"#ee6e73"}}>Ver Detalle</Button>
                        </Link>
                    </Col>
                </Row>
                <Row key={product.id + "_3"} className="center-align">
                    <Col s={12} m={12} l={12} >
                        <span>{"Stock disponible " + product.stockAvailable}</span>
                    </Col>
                </Row>
            </Card>
        </Col>
    )
}

export default Item