import CartWidget from './CartWidget';
import { Link } from 'react-router-dom'
import { useCartContext } from '../context/CartContext';

function NavBar() {
    const { cartList } = useCartContext()
    return (
        <nav>
            <div className="nav-wrapper">
                <Link to="/" className="brand-logo">Cachogos</Link>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
                    <li><Link to="/alimento/perros" className="waves-effect waves-light">Perros</Link></li>
                    <li><Link to="/alimento/gatos" className="waves-effect waves-light">Gatos</Link></li>
                    {cartList.length > 0 && <li><Link to="/cart" className="waves-effect waves-light"><CartWidget /></Link></li>}
                </ul>
            </div>
        </nav>
    )
}

export default NavBar;