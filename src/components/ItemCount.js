import { useState } from "react"
import { Card, Icon, Row, Col, Button  } from "react-materialize"

function ItemCount( { stockAvailable, initial, onAdd } ) {

    const [stock, setStock] = useState( initial )

    const handleAddClick = () => {
        if ( stock < stockAvailable ) {
            setStock( stock + 1 )
        }
    }
    
    const handleRemoveClick = () => {
        if ( stock > 1 ) {
            setStock( stock - 1 )
        }
    }

    return (
        <>
            <Row key="1">
                <Col key="1" l={1}>
                    <a href="#" onClick={ () => handleRemoveClick() } style={{color:"#ee6e73"}} ><Icon>remove_circle</Icon></a>
                </Col>
                <Col key="2" l={1}>
                    <span>{ stock }</span>
                </Col>
                <Col key="3" l={1}>
                    <a href="#" onClick={ () => handleAddClick() } style={{color:"#ee6e73"}}><Icon>add_circle</Icon></a>
                </Col>
            </Row>
            <Row key="2">
                <Col l={12}>
                    <Button onClick={ () => onAdd( stock ) } style={{background:"#ee6e73"}}>Agregar al Carrito</Button>
                </Col>
            </Row>
        </>
    )
}

export default ItemCount