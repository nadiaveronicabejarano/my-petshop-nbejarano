# Cachagos petshop

Este proyecto permite la venta de productos veterinarios. 

## Tecnologias utilizadas

* [React JS](https://es.reactjs.org/), biblioteca de javascript para crear interfaces de usuario.
* [Materialize](http://react-materialize.github.io/react-materialize/?path=/story/react-materialize--welcome), componentes que utilizan material design para reactjs.
* [React router dom](https://www.npmjs.com/package/react-router-dom), libreria utilizada para el enrutado en reactjs.

## Comandas para levantar el proyecto

### `npm install` 
Comando para instalar todas las dependencias del proyecto

### `npm run start`
Comando para correr la aplicación en un ambiente local

## Url local
**localhost:3000**